package com.programandoenjava.desafiomarzo2024.rooms;

public enum RoomTypes {
    SINGLE,
    DUO,
    PRO,
    DELUXE
}
