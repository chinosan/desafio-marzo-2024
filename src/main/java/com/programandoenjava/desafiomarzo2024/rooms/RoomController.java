package com.programandoenjava.desafiomarzo2024.rooms;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/room")
public class RoomController {
    public List<Room> allRooms = new ArrayList<>();

    @GetMapping
    public ResponseEntity<List<Room>> getAllRooms() {
        return ResponseEntity.ok(allRooms);
    }

    @PostMapping
    public ResponseEntity<Room> addRoom(@RequestBody Room room) {
        allRooms.add(room);
        int i = allRooms.size();
        return ResponseEntity.ok(allRooms.get(i-1));
    }

    @PutMapping
    public ResponseEntity<Room> updateRoom(@RequestBody Room room) {
        int index = allRooms.indexOf(room);
        allRooms.add(index, room);
        return ResponseEntity.ok(allRooms.get(index));
    }
}
